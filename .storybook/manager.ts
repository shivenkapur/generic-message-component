import { create } from '@storybook/theming/create';
import { addons } from '@storybook/addons';

const theme = create({
  base: 'light',

  colorSecondary: '#00AF7C',

  appBg: '#F8F8F8',
  appBorderColor: '#EDEDED',
  appBorderRadius: 6,

  barTextColor: '#999999',
  barSelectedColor: '#00AF7C',
  barBg: '#F2F2F2',

  inputBg: 'white',
  inputBorder: 'rgba(0,0,0,.1)',
  inputTextColor: '#333333',
  inputBorderRadius: 4,

  brandTitle: 'Set Sail Software',
  brandUrl: 'https://chatbot.com.hk/',
  brandImage: 'set-sail-logo.png',
});

addons.setConfig({
  theme,
  panelPosition: 'right',
});
