module.exports = {
  stories: ['../src/**/**/*.stories.@(tsx|mdx)'],
  webpackFinal: config => {
    return config;
  },
  addons: [
    '@storybook/addon-knobs',
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-viewport',
    '@storybook/addon-a11y',
    '@storybook/addon-docs',
    'storybook-addon-jsx',
  ],
};
