/* eslint-disable */
import React from 'react';
import conversationData from './fixtures/conversation';
import { Message } from './components/message';
import { Box, BoxProps } from 'rebass/styled-components';

interface File {
  file: string;
}

interface Image {
  url: string;
}

interface Text {
  text: string;
}

export interface MessageData {
  id: string;
  content: File | Image | Text;
  contentType: string;
  senderType: string;
  createdAt: Date;
}

function App() {
  return (
    <Box backgroundColor="#f0f5f8" m="0 20%" p="20px" overflow="scroll">
      {conversationData.map(message => (
        <Message
          key={message.id}
          imgSrc={message.content.url || undefined}
          fileSrc={message.content.file || undefined}
          text={message.content.text || undefined}
          time={new Date(message.createdAt)}
          variant={message.senderType === 'agent' ? 'sent' : 'received'}
        />
      ))}
    </Box>
  );
}

export default App;
