import React from 'react';
import { Image, Text, BoxProps } from 'rebass/styled-components';
import { MessageCard } from './message-card';
import { File } from './file';

export interface Message {
  time: Date;
  text?: string;
  imgSrc?: string;
  fileSrc?: string;
}

export const Message: React.FC<BoxProps & Message> = ({
  time,
  text,
  imgSrc,
  fileSrc,
  ...props
}) => {
  return (
    <MessageCard {...props}>
      <MessageCard.Content data-testid="content">
        {imgSrc ? <Image src={imgSrc} /> : null}
        {fileSrc ? <File src={fileSrc} /> : null}
        {text ? <Text>{text}</Text> : null}
      </MessageCard.Content>
      <MessageCard.Footer data-testid="footer">{time.toUTCString()}</MessageCard.Footer>
    </MessageCard>
  );
};

Message.defaultProps = {
  variant: 'sent',
};
