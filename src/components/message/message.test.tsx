/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { render } from '@testing-library/react';
import React from 'react';
import { Message } from './message';

const time = new Date(1606089351583);

describe('Message', () => {
  it('should show a text message with the correct timestamp', () => {
    const { getByTestId } = render(<Message text="This is a test message!" time={time} />);
    const content = getByTestId('content');

    expect(content.textContent).toBe('This is a test message!');
  });

  it('should show an image message', () => {
    const imageSource = 'https://source.unsplash.com/random';
    const { getByTestId } = render(<Message imgSrc={imageSource} time={time} />);
    const content = getByTestId('content');

    expect(content.firstChild).toHaveAttribute('src', imageSource);
  });

  it('should show a file message', () => {
    const fileSource =
      'https://drive.google.com/uc?export=download&id=1ft-mFQ8ER27jBQJrE2SGdaIsKjnl8_cu';
    const { getByTestId } = render(<Message fileSrc={fileSource} time={time} />);
    const content = getByTestId('content');

    const aTag = content.querySelector('a');

    expect(aTag).toHaveAttribute('href', fileSource);
  });

  it('should show a human readable time stamp', () => {
    const { getByTestId } = render(<Message text="This is a test message!" time={time} />);
    const footer = getByTestId('footer');

    expect(footer.textContent).toBe(time.toUTCString());
  });
});
