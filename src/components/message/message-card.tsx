/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable react/display-name */
import React from 'react';
import { Box, BoxProps } from 'rebass/styled-components';

export type MessageComponent<P = {}> = {
  (props: P): JSX.Element;
  defaultProps?: Partial<P>;
  children?: React.ReactNode;
  Header: React.FC;
  Content: React.FC;
  Footer: React.FC;
};

export const MessageCard: MessageComponent<BoxProps> = ({ variant, children, ...props }) => {
  return (
    <Box tx="message" variant={variant} {...props}>
      {React.Children.map(children, child => {
        return React.isValidElement(child) ? React.cloneElement(child, { variant }) : child;
      })}
    </Box>
  );
};

MessageCard.Header = ({ children, ...props }) => {
  return (
    <Box tx="message.header" {...props}>
      {children}
    </Box>
  );
};

MessageCard.Content = ({ children, ...props }) => {
  return (
    <Box tx="message.content" {...props}>
      {children}
    </Box>
  );
};

MessageCard.Footer = ({ children, ...props }) => {
  return (
    <Box tx="message.footer" {...props}>
      {children}
    </Box>
  );
};
