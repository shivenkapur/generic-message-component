import React from 'react';
import { Flex, Box, Link, Text, BoxProps } from 'rebass/styled-components';
import { ReactComponent as DownloadIcon } from './images/download.svg';

export interface FileMessageProps extends BoxProps {
  src: string;
}

export const File: React.FC<FileMessageProps> = ({ src, ...props }) => {
  return (
    <Flex backgroundColor="rgba(220, 231, 240, 0.3)" padding="10px" {...props}>
      <Box ml="auto">
        <Link download="filename" href={src}>
          <Flex alignItems="center">
            <Text mr={1}>Click here to download</Text>
            <DownloadIcon height="30px" />
          </Flex>
        </Link>
      </Box>
    </Flex>
  );
};
