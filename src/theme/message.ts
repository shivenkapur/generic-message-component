const root = {
  sent: {
    backgroundColor: 'primary',
    borderRadius: '13px 5px 5px 13px',
    width: 'fit-content',
    minWidth: '100px',
    maxWidth: '300px',
    padding: '10px 15px',
    border: '1px solid #f2f2f2',
    margin: '10px 20px 0 auto',
  },
  received: {
    variant: 'message.sent',
    backgroundColor: 'white',
    marginLeft: '20px',
  },
};

const content = {
  sent: {
    fontSize: '14px',
    color: 'white',
  },
  received: {
    variant: 'message.content.sent',
    color: 'darkGray',
  },
};

const footer = {
  sent: {
    fontSize: '12px',
    color: '#a3cce8',
    marginTop: '10px',
  },
  received: {
    variant: 'message.footer.sent',
    color: '#7c8ba5',
  },
};

export default {
  ...root,
  content,
  footer,
};
