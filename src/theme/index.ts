import { createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';
import colors from './colors';
import message from './message';

export const GlobalStyle = createGlobalStyle`
  ${normalize}

  html, body {
    font-family: 'Open Sans', Helvetica, Arial, sans-serif;
    padding: 0;
    font-size: 14px;
  }

  a {
    font-style: none;
    text-decoration: none; 
    color: #333333;
  }
`;

const theme = {
  fonts: {
    body: 'Roboto, Helvetica, Arial, sans-serif, sans-serif',
    heading: 'inherit',
    monospace: 'Menlo, monospace',
  },

  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 96],

  radii: {
    default: 4,
    card: 6,
    circle: 99999,
  },

  fontWeights: {
    body: 400,
    heading: 700,
    semiBold: 600,
    bold: 700,
  },

  space: [0, 4, 8, 16, 32, 64, 128, 256, 512],

  colors,

  styles: {
    root: {
      fontFamily: 'body',
      fontWeight: 'body',
      lineHeight: 'body',
    },
  },

  message,
};

export default theme;
